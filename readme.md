# WAN CHECKER

A simple little node script which will ping the most popular websites located in North America every 5 minutes and log if they were reached when attempting to ping them or not. 

### Why make this?

This was something I made when I was fighting with my ISP (as we all have) about whether me losing internet connection periodically was a fault on my end in my hardware, or on their end in their hardware/network. I had tried everything I could on my end but the issue still persisted.

Finally I just plugged a laptop I had with an ethernet cable directly into the modem and let this script run where you could see it attempt to ping various websites every 5 minutes with no other hardware barriers in the way. Just laptop, to modem, to ISPs greater infrastructure.

It still took some finagling to get them to really address the problem but seeing the times the connection roughly came on or offline was something the tech said he found helpful when he came out.

## Installation & Usage

* Have [Node.js](https://nodejs.org/en/) installed on your system.
* Make sure you have [npm installed](https://www.npmjs.com/get-npm) as well. It should install along side Node but never hurts to check.
* Clone/download this repository to your computer.
* Install all the node modules you need with `npm install`
* Start the script with `node main.js`
* Let it run as long as you need it to. Press `ctrl+c` to haul execution at any time.

As soon as you run the script you should see it immediately execute a ping test to the entire domain list. Those pings should then be written to a `pings.log` file in the route directory of this application. You can check that log file for some detail on each ping and if it was successful or not.

After that the script will ping the domains every 5 minutes. There will be some console output just to indicate this is all still happening but the best place to read the status of pings over time is going to be the log file.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[CUSTOM](license.md)