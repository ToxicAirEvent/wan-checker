# LICENSE

A quick explanation of what you can do with this software for.

## Usage and Modification
This software is provided for free and may be used by private, public, and commercial entities as they see fit. This software may not be sold, or bundled into software which is sold for proprietary use or monetary gain.

Modification to the application is freely allowed and may be redistributed under the following guidelines outlined in the paragraph above.

## Warranty

This software is provided as is with no warranty. In your usage and modification of it you assume all risk and responsibility for any damage.

## Commercial Distribution
If you would like to distribute this software commercially for monetary gain please contact [jackemceachern@gmail.com](mailto:jackemceachern@gmail.com) for distribution licensing information.

## Changes
This license is subject to change without notification at any time.