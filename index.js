const express = require('express');
const cors = require('cors');
var cron = require('node-cron');

const ping_serv = require('./ping-service.js');

/*
	A list of the most popular websites in the North American region. (https://en.wikipedia.org/wiki/List_of_most_popular_websites)
	You can modify this list for to be any set of domains you wish. I just live in the US so these are sites commonly visited here which should be easily reachable.

	Also included the Google and Cloudflare DNS servers since those route and resolve large swaths of traffic and should both be reachable.
*/
const domainsList = [
	"8.8.8.8",
	"8.8.4.4",
	"1.1.1.1",
	"1.0.0.1",
	"google.com",
	"www.youtube.com",
	"facebook.com",
	"www.yahoo.com",
	"wikipedia.org",
	"amazon.com",
	"live.com",
	"reddit.com",
	"twitter.com",
	"blogger.com",
	"worldometers.info",
	"stackoverflow.com",
	"www.ebay.com"
];

//Creates an object that we can store historical ping data within and query from since the program has been running.
var ping_history = {};

for(let d = 0; d < domainsList.length; d++){
	ping_history[domainsList[d]] = {last: null, history: []};
}

//Create a CRON job to ping all of the things on the domain list every 5 minutes.
cron.schedule('*/5 * * * *', async () => {
	for(let p = 0; p < domainsList.length; p++){
		let ping_response = await ping_serv.pingDomain(domainsList[p]);


		ping_history[domainsList[p]]['history'].unshift(ping_response);

		//Only log the last 25 pings.
		if(ping_history[domainsList[p]]['history'].length > 25){
			ping_history[domainsList[p]]['history'].pop();
		}
		
		ping_history[domainsList[p]]['last'] = ping_response.alive;
	}
});

//Set up the Express routing for the app.
var app = express();

app.use(cors());

app.get('/', function (req, res) {
  	res.send('Up and running.');
});

app.get('/all-sites', function(req, res){
	res.json(ping_history);
});

app.get('/site-history/:domain_name', function(req, res){

	let domain_params = req.params;

	if(!ping_history.hasOwnProperty(domain_params.domain_name)){
		res.send(false);
	}

	res.json(ping_history[domain_params.domain_name]);
});

app.listen(3001);