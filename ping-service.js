//npm packages needed to make all this work.
var ping = require('ping');
const log = require('simple-node-logger').createSimpleLogger('pings.log');

/*
	Function attempts to ping the domain fed to it.
	Logs the results of all ping requests. Also returns an object of some data about the result of the ping which can be used elsewhere within the program.
*/
async function pingDomain(domain){

	//Form a date and time string that is easier to read than what the logger puts out by default.
	var dt = new Date();
	var dateString = dt.getMonth() + "/" + dt.getDate() + "/" + dt.getFullYear() + " | " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

	let pingRes = await ping.promise.probe(domain,{timeout: 10});

	//By default set everything as if it's failing and update if it successfully is pinged.
	let ping_data = {date_time: dateString, alive: false, resolved: null};

	if(pingRes.alive == true){
		ping_data.alive = true;
		ping_data.resolved = pingRes.host;

		let successInfo = dateString + " " + domain + " (Resolved: " + pingRes.host + ") was successfully pinged.";
		log.info(successInfo);
	}else{
		//The 'ping' package doesn't provide a ton of info on failure so the message is going to have to be very generic like this.
		let failInfo = dateString + " ERROR PINGING HOST: " + domain;
		log.error(failInfo);
	}

	console.log("Finished attempting to ping: " + domain);

	return ping_data;
}

exports.pingDomain = pingDomain;